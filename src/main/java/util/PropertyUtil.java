package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static base.ProjectConstant.configFilePath;

public class PropertyUtil {
    private static Properties prop = new Properties();
    private static FileInputStream inputStream = null;

    static {
        File file = new File(configFilePath);
        try {
            inputStream = new FileInputStream(file);
            prop.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getProperty(String key) {
        return prop.getProperty(key);
    }

}
