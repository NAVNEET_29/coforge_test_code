package base;

import io.restassured.path.json.JsonPath;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Base implements ProjectConstant {
    public String getJsonValue(String rsBody, String path) {
        JsonPath jsonPath = new JsonPath(rsBody);
        return jsonPath.getString(path);
    }


    public String getLatestDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        String result = s.format(new Date(cal.getTimeInMillis()));
        return result;
    }
}
