package base;

public interface ProjectConstant {
    String configFilePath=System.getProperty("user.dir")+"\\src\\main\\resources\\config.properties";
    String RESOURCE_LATEST ="/api/latest";
    String RESOURCE_DATE ="/api/2010-12-28";
    String BASE_RATE_DEFAULT="EUR";
    String DEFAULT_EXPECTED_DATE="2020-12-24";
    String OLD_EXPECTED_DATE="2010-12-28";
    String EXPECTED_EUR_INR_RATE="89.6845";
    String EXPECTED_USD_INR_RATE="73.554085";
}
