package stepDefinition;

import base.Base;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;
import util.PropertyUtil;

import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class ExchnageRateValidationSteps extends Base {
    public static Response response;
    public static String responseBody;
    public static RequestSpecification rs;

    //Set the baseuri and rerquest spec value
    @Given("Base URL for the currency exchange rate API")
    public void base_URL_for_the_currency_exchange_rate_API() {
        RestAssured.baseURI = PropertyUtil.getProperty("BaseURLForeignExchangeRate");
        rs = given().log().all();
    }

    // send get requst api to with the latest date and store the response and responseBody as string
    @When("Hit the GET request with latest date supplied to baseurl")
    public void hit_the_GET_request_with_latest_date_supplied_to_baseurl() {
        response = rs.when().log().all().get(RESOURCE_LATEST);
        responseBody = response.asString();
    }

    //Validating the response code
    @Then("Validate the Status code of the response should be {int}")
    public void validate_the_Status_code_of_the_response_should_be(Integer statusCode) {
        response.then().log().all().assertThat().statusCode(statusCode);
    }

    //validating the base currency code in the response body
    @Then("The base rate should be {string} in the responseBody")
    public void the_base_rate_should_be_in_the_responseBody(String baseRate) {
        Assert.assertEquals(getJsonValue(responseBody, "base"), baseRate);
    }

    //validating the currency conversion date
    @Then("Exchange rate date should be of current date")
    public void exchange_rate_date_should_be_of_current_date() {
        String actualDate = getJsonValue(responseBody, "date");
        String expectedDate = DEFAULT_EXPECTED_DATE;
        Assert.assertEquals(actualDate, getLatestDate());
    }

    //setting the query parameter in the api request
    @Given("Set the query parameter as below")
    public void set_the_query_parameter_as_below(DataTable queryParam) {
        List<Map<String, String>> dataMaps = queryParam.asMaps(String.class, String.class);
        for (Map<String, String> dataMap : dataMaps) {
            rs.queryParam(dataMap.get("KEY"), dataMap.get("VALUE"));
        }

    }

    //Validating the INR rate agaisnt Euro
    @Then("Correct INR rate against EURO should be displayed")
    public void correct_INR_rate_against_EURO_should_be_displayed() {
        String actualInrRate = getJsonValue(responseBody, "rates.INR");
        Assert.assertEquals(actualInrRate, EXPECTED_EUR_INR_RATE);
    }

    //Validating the INR rate agaisnt USD
    @Then("Correct INR rate against USD should be displayed")
    public void correct_INR_rate_against_USD_should_be_displayed() {
        String actualInrRate = getJsonValue(responseBody, "rates.INR");
        Assert.assertEquals(actualInrRate, EXPECTED_USD_INR_RATE);
    }

    //Sending the hit request api with old date
    @When("Hit the GET request with old date supplied to baseurl")
    public void hit_the_GET_request_with_old_date_supplied_to_baseurl() {
        response = rs.when().log().all().get(RESOURCE_DATE);
        responseBody = response.getBody().asString();
    }

    //Validating the exchnage rate against old dated
    @Then("Exchange rate date should be same as the date requested")
    public void exchange_rate_date_should_be_same_as_the_date_requested() {
        Assert.assertEquals(getJsonValue(responseBody, "date"), OLD_EXPECTED_DATE);
    }

    //Setting up query parameter by passing argument
    @When("Set the query parameter as {string} against {string}")
    public void set_the_query_parameter_as_against(String string, String string2) {
        rs.queryParam(string2, string);
    }

    //Validating the exhnage rate conversion with expected and actual
    @Then("validate that the exchange rate conversion against the {string} should be {string}")
    public void validate_that_the_exchange_rate_conversion_against_the_same_country_should_be(String string, String string2) {
        Assert.assertEquals(getJsonValue(responseBody, "rates." + string + ""), string2);
    }

    //Validating the exchange rate of target country with respect to Euro
    @Then("validate that the exchange rate of Euro against the {string} should be {string}")
    public void validate_that_the_exchange_rate_of_Euro_against_the_target_country_should_be(String string, String string2) {
        String value = getJsonValue(responseBody, "rates." + string + "");
        Assert.assertEquals(String.format("%.1f", Double.valueOf(value)), String.format("%.1f", Double.valueOf(string2)));

    }

    //validating the error message when incorrect endpoint is hit
    @Then("Error message should be displayed as {string}")
    public void error_message_should_be_displayed_as(String string) {
        Assert.assertEquals(getJsonValue(responseBody, "error"), string);
    }

    //Setting up multiple query parameter
    @When("Set the query parameter as {string} against {string} and {string} against {string}")
    public void set_the_query_parameter_as_against_and_against(String string, String string2, String string3, String string4) {
        rs.queryParam(string2, string).queryParam(string4, string3);
    }

    //Validating the exchnage rate of Base and target country
    @Then("validate that the exchange rate of BaseCurrency against the {string} should be {string}")
    public void validate_that_the_exchange_rate_of_BaseCurrency_against_the_should_be(String string, String string2) {
        String actualValue = String.format("%.1f", Double.valueOf(getJsonValue(responseBody, "rates." + string + "")));
        Assert.assertEquals(actualValue, String.format("%.1f", Double.valueOf(string2)));
    }

    //Validate the status code as passed in args
    @Then("Validate the Status code of the response should be {string}")
    public void validate_the_Status_code_of_the_response_should_be(String string) {
        Assert.assertTrue(response.getStatusCode() == Integer.valueOf(string));
    }

}
