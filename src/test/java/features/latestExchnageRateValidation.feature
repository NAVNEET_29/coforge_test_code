Feature: Validate the latest exchange rate

  Background: Set the BaseURI for the for the endpoint
    Given Base URL for the currency exchange rate API

  @UAT @Regression @NewRates
  Scenario: Validate that the base exchange rate should be Euro as the default base rate
    When Hit the GET request with latest date supplied to baseurl
    Then Validate the Status code of the response should be 200
    And  The base rate should be "EUR" in the responseBody
    And  Exchange rate date should be of current date

  @BaseCurrency @Regression @NewRates
  Scenario Outline: Validate the API response against the different Base countries
    When Set the query parameter as "<BaseCurrency>" against "base"
    And Hit the GET request with latest date supplied to baseurl
    Then Validate the Status code of the response should be 200
    And  The base rate should be "<BaseCurrency>" in the responseBody
    And  Exchange rate date should be of current date
    And validate that the exchange rate conversion against the "<BaseCurrency>" should be "1.0"

    Examples:
      | BaseCurrency |
      | GBP          |
      | INR          |
      | AUD          |
      | HKD          |
      | NZD          |

  @TargetCurrency @Regression @NewRates
  Scenario Outline: Validate the API response of Euro (default base) against the different countries
    When Set the query parameter as "<TargetCurrency>" against "Symbols"
    And Hit the GET request with latest date supplied to baseurl
    Then Validate the Status code of the response should be 200
    And  The base rate should be "EUR" in the responseBody
    And  Exchange rate date should be of current date
    And validate that the exchange rate of Euro against the "<TargetCurrency>" should be "<RateAgainstEuro>"

    Examples:
      | TargetCurrency | RateAgainstEuro |
      | GBP            | 0.90            |
      | INR            | 89.94           |
      | AUD            | 1.60            |
      | HKD            | 9.47            |
      | NZD            | 1.71            |

  @BaseCurrency @TargetCurrency @UAT @NewRates
  Scenario Outline: Validate the API response of different base countries against the different countries
    When Set the query parameter as "<TargetCurrency>" against "Symbols" and "<BaseCurrency>" against "base"
    And Hit the GET request with latest date supplied to baseurl
    Then Validate the Status code of the response should be 200
    And  The base rate should be "<BaseCurrency>" in the responseBody
    And  Exchange rate date should be of current date
    And  validate that the exchange rate of BaseCurrency against the "<TargetCurrency>" should be "<Rates>"

    Examples:
      | BaseCurrency | TargetCurrency | Rates    |
      | GBP          | IDR            | 19177.13 |
      | INR          | ILS            | 0.04     |
      | AUD          | GBP            | 0.56     |
      | HKD          | SGD            | 0.17     |
      | NZD          | NZD            | 1.00     |

  @InvalidScenerio @NewRates
  Scenario Outline: Validate results when incorrect/invalid endpoint is invoked
    When Set the query parameter as "<BaseCurrency>" against "base"
    And Hit the GET request with latest date supplied to baseurl
    Then Validate the Status code of the response should be "<StatusCode>"
    And Error message should be displayed as "<ErrorMessage>"

    Examples:
      | BaseCurrency | StatusCode | ErrorMessage                 |
      | BKG          | 400        | Base 'BKG' is not supported. |
      | 123          | 400        | Base '123' is not supported. |
      | @#$          | 400        | Base '@#$' is not supported. |