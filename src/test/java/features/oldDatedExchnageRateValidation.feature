Feature: Validate the old dated exchange rate
@OldRates @UAT
Scenario: Get the current exchange rates for different currencies against the Euro
  Given Base URL for the currency exchange rate API
  When Hit the GET request with old date supplied to baseurl
  Then Validate the Status code of the response should be 200
  And  The base rate should be "EUR" in the responseBody
  And  Exchange rate date should be same as the date requested

  @OldRates @UAT
  Scenario: Get the current exchange rates for Indian currency against the Euro
    Given Base URL for the currency exchange rate API
    And Set the query parameter as below
      | KEY     | VALUE |
      | symbols | INR   |
    And Hit the GET request with old date supplied to baseurl
    Then Validate the Status code of the response should be 200
    And  The base rate should be "EUR" in the responseBody
    And  Correct INR rate against EURO should be displayed
    And  Exchange rate date should be same as the date requested

  @OldRates @Regression
  Scenario: Get the current exchange rates for different currencies against the USD
    Given Base URL for the currency exchange rate API
    When Hit the GET request with old date supplied to baseurl
    Then Validate the Status code of the response should be 200
    And  The base rate should be "USD" in the responseBody
    And  Exchange rate date should be same as the date requested

  @OldRates @Regression
  Scenario: Get the current exchange rates for Indian currency against the USD
    Given Base URL for the currency exchange rate API
    And Set the query parameter as below
      | KEY     | VALUE |
      | symbols | INR   |
      | base    | USD   |
    And Hit the GET request with old date supplied to baseurl
    Then Validate the Status code of the response should be 200
    And  The base rate should be "USD" in the responseBody
    And  Correct INR rate against USD should be displayed
    And  Exchange rate date should be same as the date requested
